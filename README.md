#heritabillity_estimation

The code for this project is included in the script heritability_estimation.R

To start running the code, clone the code from the github repository link below; The command line below in terminal can be used to clone the repository;


```
    git clone https://gitlab.msu.edu/lesiyonr/heritabillity_estimation.git
```

After getting the code from the repository, a folder named heritabillity_estimation should appear in your current working repository. Navigate to the repository. Inside the repository it includes; 

    ** data folder; Contain the GWAS summary statistics for Bipolar disoder
    ** report pdf; the report for project; 
    ** heritability_estimation.R used to conduct the analysis for this project. 

Before running the R script excecute the setup_bash_file data for downloadig pre-computed LD score and place them in the appropriate folder.
```
    bash setup.sh
```


